# Traffic-Flow-Prediction
PyTorch implementation of [ST-Resnet](https://github.com/prabinrath/Traffic-Flow-Prediction/blob/main/Papers/1701.02543.pdf). This repository has pre-processed dataset as `.npy` files in the `Datasets` folder. Our custom implementation results are close to what was presented in the original paper.
